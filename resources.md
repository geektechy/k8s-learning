[architecture](https://medium.com/geekculture/k8s-cluster-internal-architecture-2b1489f8ce28)

[kodecloud repo](https://github.com/kodekloudhub/certified-kubernetes-administrator-course)

[minikube - from k8s documentation](https://kubernetes.io/docs/tutorials/hello-minikube/)

[Kubernetes hard way -kelseyhightower](https://github.com/kelseyhightower/kubernetes-the-hard-way)

[setting up a docker private registry](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-20-04)